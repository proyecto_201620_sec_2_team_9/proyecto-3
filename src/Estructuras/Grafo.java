package Estructuras;

import Mundo.Ciudad;
import Mundo.Vuelo;

public class Grafo<V extends Nodo, E extends Arco> 
{

	/**
	 * Nodos del grafo
	 */
	private V nodos[]; 
	private E arcos[];
	private V[][] matrizAdyacenciaNacional;
	private boolean[][] visitados;
	private boolean[][] visitadosGeneral;
	private boolean[][] visitadosAntigua;
	private int filasMatriz;
	private int columnasMatriz;

	private int cantidadDeVertices;
	int cantidadDeArcos;

	private int ultimoVertices;
	private int ultimoArcos;
	String ciudadesdfs;
	Tupla[][] rutas;

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public Grafo() 
	{
		cantidadDeVertices = 0;
		if(cantidadDeVertices != 0)
			inicializarVertices();
	}

	public void inicializarVertices()
	{
		nodos = (V[]) new Ciudad[cantidadDeVertices*2];
		ultimoVertices = 0;
	}

	public boolean agregarNodo(V nodo) 
	{
		boolean agrego = false;
		nodos[ultimoVertices] = nodo;
		agrego = true;
		ultimoVertices++;
		return agrego;
	}

	//El id es el nombre de la ciudad
	public boolean eliminarNodo(String id) 
	{
		boolean elimino = false;
		for (int i = 0; i < nodos.length && !elimino; i++) 
		{
			Nodo nodoActual = nodos[i];
			if(nodoActual != null)
			{
				if(nodoActual.darId().equals(id))
				{
					nodos[i] = null;
					elimino = true;
					cantidadDeVertices--;
				}
			}
		}
		return elimino;
	}

	public Arco[] darArcos() 
	{
		//Arco[] totalConexiones = new Arco[FALTAA];
		int pointer = 0;
		for (int i = 0; i < nodos.length; i++) 
		{
			/**FALTAAAAEstacion estActual = (Estacion) nodos[i];
			if(estActual.darAdyacentes() != null)
			{
				for (int j = 0; j < estActual.darAdyacentes().length; j++) 
				{
					totalConexiones[pointer] = estActual.darAdyacentes()[j];
					pointer++;
				}
			}*/
		}
		//return totalConexiones;
		return null;
	}

	public Nodo[] darNodos() 
	{
		return nodos;
	}

	public Nodo buscarNodo(String ciudadOrigen) 
	{
		Nodo buscado = null;
		boolean encontro = false;
		if(nodos != null)
		{
			for (int i = 0; i < nodos.length && !encontro; i++) 
			{
				if(nodos[i] != null)
				{
					if(nodos[i].darId().equals(ciudadOrigen) || nodos[i].darId().equals(ciudadOrigen.toUpperCase()))
					{
						buscado = nodos[i];
						encontro = true;
					}
				}
			}
		}
		return buscado;
	}

	public void guardarTamanioVertices(int tamanio) 
	{
		cantidadDeVertices = tamanio;
		filasMatriz = cantidadDeVertices;
		columnasMatriz = cantidadDeVertices;
		inicializarVertices();
		matrizAdyacenciaNacional = (V[][]) new Ciudad[filasMatriz][columnasMatriz];
	}
	public void guardarTamanioArcos(int tamanio) 
	{
		cantidadDeArcos = tamanio;
		inicializarArcos();
	}

	public void inicializarArcos() 
	{
		arcos = (E[]) new Vuelo[cantidadDeArcos*2];
		ultimoArcos = 0;
	}

	public boolean agregarArco(E arco)
	{
		boolean agrego = false;
		arcos[ultimoArcos] = arco;
		if(((Vuelo) arco).darTipoVuelo().equals("Nacional"))
		{
			agregarVerticesAMatrizDeAdyacencia(arco.darNodoInicio(), arco.darNodoFin());
		}

		agrego = true;
		ultimoArcos++;
		return agrego;
	}

	public void agregarVerticesAMatrizDeAdyacencia(Nodo nodoInicio, Nodo nodoFin) 
	{
		String nombreCiudadInicio = (String) nodoInicio.darId();
		String nombreCiudadDestino = (String) nodoFin.darId();
		int posicion = -1;
		boolean encontro = false;
		for (int i = 0; i < nodos.length && !encontro; i++) 
		{
			if(nodos[i] != null)
			{
				if(nodos[i].darId().equals(nombreCiudadInicio))
				{
					posicion = i;
					encontro = true;
				}
			}
		}
		matrizAdyacenciaNacional[posicion][0] = (V) new Ciudad(nombreCiudadInicio);
		boolean agrego = false;
		for (int k = 0; k < columnasMatriz && !agrego; k++) 
		{
			if(matrizAdyacenciaNacional[posicion][k] != null)
			{
				if(matrizAdyacenciaNacional[posicion][k].darId().equals(nombreCiudadDestino))
					agrego = true;
			}
			if(matrizAdyacenciaNacional[posicion][k] == null && !agrego)
			{
				matrizAdyacenciaNacional[posicion][k] = (V) new Ciudad(nombreCiudadDestino);
				agrego = true;
			}
		}
	}


	public void eliminarArco(int id) 
	{
		for (int i = 0; i < arcos.length; i++) {
			if(arcos[i] != null)
			{
				if(((Vuelo) arcos[i]).darId() == id)
				{
					arcos[i] = null;
					cantidadDeArcos--;
				}
			}
		}
	}

	public String[][] dfs()
	{
		colocarAdyacentes();
		boolean sePuede = true;
		String[][] matrizDevolver = new String[300][columnasMatriz];
		int dondeVa = 0;
		visitadosGeneral = new boolean[filasMatriz][columnasMatriz];
		String antigua= " ";
		int x = 0;
		while(x < 90)
		{
			while(sePuede)
			{
				visitados = new boolean[filasMatriz][columnasMatriz];
				ciudadesdfs = " ";
				darComponentesConectados(x,0);
				visitadosAntigua = visitados;
				if(antigua.equals(ciudadesdfs))
				{
					sePuede = false;
				}

				antigua = ciudadesdfs;
				String[] compConectados = ciudadesdfs.split(", ");
				for (int i = 0; i < compConectados.length; i++) {
					matrizDevolver[dondeVa][i] = compConectados[i];	
				}
				dondeVa++;
			}
			x++;
			sePuede = true;
		}
		return matrizDevolver;
	}
	private String darComponentesConectados(int fila, int columna) 
	{
		if(!visitados[fila][columna])
		{
			int filaAct = buscarComoOrigen(matrizAdyacenciaNacional[fila][columna]);
			if(filaAct != -1 && visitadosAntigua != null)
			{
				V[] adj = (V[]) ((Ciudad) matrizAdyacenciaNacional[filaAct][0]).darAdyacentes();
				boolean todosVisitados = false;
				for (int i = 0; i < adj.length && adj[i] != null; i++) {
					if(visitadosAntigua[filaAct][i])
						todosVisitados = true;
					else
						todosVisitados = false;
				}
				if(todosVisitados)
				{
					visitadosGeneral[fila][columna] = true;
				}
			}

			visitados[fila][columna] = true;
			if(matrizAdyacenciaNacional[fila][columna] != null)
			{
				String[] ciud = ciudadesdfs.split(", ");
				boolean noSepuede = false;

				for (int i = 0; i < ciud.length && !noSepuede; i++) {
					if(ciud[i].equals(matrizAdyacenciaNacional[fila][columna].darId()) || ciud[i].substring(1).equals(matrizAdyacenciaNacional[fila][columna].darId()) || visitadosGeneral[fila][columna])
						noSepuede = true;
				}
				if(!noSepuede)
					ciudadesdfs = ciudadesdfs +  matrizAdyacenciaNacional[fila][columna].darId() +  ", " ;
			}
		}
		if(matrizAdyacenciaNacional[fila][columna] == null)
		{
			return darComponentesConectados(fila+1, 0);
		}
		int filaDondeEsta = buscarComoOrigen(matrizAdyacenciaNacional[fila][columna]);
		if(filaDondeEsta == -1)
		{
			return (String) matrizAdyacenciaNacional[fila][columna].darId();
		}
		else
		{
			if(filaDondeEsta == fila || visitadosGeneral[fila][columna])
			{
				return darComponentesConectados(fila, columna+1);
			}
			else
			{
				if(visitados[filaDondeEsta][0] == false)
					return darComponentesConectados(filaDondeEsta, 0);
				else
				{
					if(matrizAdyacenciaNacional[fila][columna+1] != null)
					{
						return darComponentesConectados(fila, columna+1);
					}
					else 
					{
						return (String) matrizAdyacenciaNacional[fila][columna-1].darId();
					}
				}
			}
		}
	}

	private void colocarAdyacentes() {
		// TODO Auto-generated method stub
		int contador = 0;
		for (int i = 0; i < filasMatriz; i++) {
			for (int j = 0; j < columnasMatriz; j++) {
				/*if(contador < 10)
				{*/
				if(matrizAdyacenciaNacional[i][j] != null)
				{
					System.out.println("fila " + i + " columna " + j + " = " + matrizAdyacenciaNacional[i][j].darId());
					if(j == 0)
					{
						int c = j+1;
						while(matrizAdyacenciaNacional[i][c] != null)
						{
							((Ciudad) matrizAdyacenciaNacional[i][j]).agregarAdyacentes((Ciudad)matrizAdyacenciaNacional[i][c]);
							c++;
						}
					}
				}
				contador++;
				//

			}
		}
	}

	private int buscarComoOrigen(V aBuscar) 
	{
		boolean devolver = false;
		int fila = -1;
		for (int i = 0; i < filasMatriz && !devolver; i++) 
		{
			if(matrizAdyacenciaNacional[i][0]!=null && aBuscar != null)
			{
				if(matrizAdyacenciaNacional[i][0].darId().equals(aBuscar.darId()))
				{
					devolver = true;
					fila = i;
				}
			}
		}
		return fila;
	}

	public String[] mstNodo(V nodoInicio) 
	{
		String[] pesosMinimos = new String[80];
		int indicador = 0;
		E[] arcos = (E[]) new Vuelo[1600]; 
		String[] marcados = new String[400];
		int count = 0;
		int dondeVa = 0;
		V nodoFinal = null;
		boolean siga = true;
		while(siga)
		{
			boolean sePuede = true;
			if(nodoFinal == null)
			{
				for (int k = 0; k < marcados.length && sePuede && marcados[k] != null; k++) {
					if(marcados[k].equals(nodoInicio.darId()))
						sePuede = false;
				}
				if(sePuede)
				{
					E[] temp = (E[]) ((Ciudad) nodoInicio).darArcos();
					for (int i = 0; i < temp.length && temp[i] != null; i++) 
					{
						arcos[dondeVa] = temp[i];
						dondeVa++;							

					}
				}	
			}
			else
			{
				E[] temp2 = (E[]) ((Ciudad) nodoFinal).darArcos();
				for (int j = 0; j < temp2.length && temp2[j] != null; j++) 
				{
					arcos[dondeVa] = temp2[j];
					dondeVa++;
				}
				boolean ok = false;
				for (int h = 0; h < temp2.length && temp2[h] != null && !ok; h++) 
				{
					V finalActual = (V) temp2[h].darNodoFin();
					for (int q = 0; q < marcados.length && !ok; q++) {
						if(marcados[q] != null)
						{
							if(!marcados[q].equals(finalActual.darId()))
								ok = true;
						}
					}
				}
				if(ok == false)
					siga = false;
			}

			int aqui = 0;
			double costoMinimo = 5000;
			for (int i = 0; i < arcos.length; i++) 
			{
				boolean caeACiclo = false;
				if(arcos[i] != null)
				{
					if(arcos[i].darCosto() < costoMinimo)
					{
						for (int j = 0; j < marcados.length && marcados[j] != null; j++) 
						{

							if((arcos[i].darNodoFin().darId() + (arcos[i].darCosto()+"")).equals(marcados[j]))
							{
								caeACiclo = true;
							}	
						}
						if(!caeACiclo)
						{
							if(!((Vuelo) arcos[i]).darTipoVuelo().equals("Int"))
							{
								costoMinimo = arcos[i].darCosto();
								nodoInicio =  (V) arcos[i].darNodoInicio();
								marcados[count] = (String)nodoInicio.darId() + (costoMinimo+"");
								count++;
								nodoFinal = (V) buscarNodo((String)arcos[i].darNodoFin().darId());
								marcados[count] = (String)nodoFinal.darId() + costoMinimo;
								aqui = i;
								count++;
							}
						}
					}
				}
			}
			if(costoMinimo < 50)
			{
				pesosMinimos[indicador] = "Ciudad origen: " + nodoInicio.darId() + ", ciudad destino: " + nodoFinal.darId() + ", costo (tiempo de vuelo) -> " +  (costoMinimo+"") + ", aerolinea: " + ((Vuelo) arcos[aqui]).darAerolinea().darNombre() + ", n�mero del vuelo: " + ((Vuelo) arcos[aqui]).darId() + ", hora del vuelo: " + ((Vuelo) arcos[aqui]).darHoraSalida();
				arcos[aqui] = null;
				indicador++;
			}
		}
		return pesosMinimos;
	}

	public String[] mstNodoInt(V nodoInicio, String dia) 
	{
		String[] pesosMinimos = new String[80];
		int indicador = 0;
		E[] arcos = (E[]) new Vuelo[1600]; 
		String[] marcados = new String[400];
		int count = 0;
		int dondeVa = 0;
		V nodoFinal = null;
		boolean siga = true;
		int cuantas = 0;
		String[] ciudadesMarcadas = new String[91];
		int ind = 0;
		while(siga)
		{
			boolean sePuede = true;
			if(nodoFinal == null)
			{
				for (int k = 0; k < marcados.length && sePuede && marcados[k] != null; k++) 
				{
					if(marcados[k].equals(nodoInicio.darId()))
						sePuede = false;
				}
				if(sePuede)
				{
					E[] temp = (E[]) ((Ciudad) nodoInicio).darArcos();
					for (int i = 0; i < temp.length && temp[i] != null; i++) 
					{
						String[] diasOper = ((Vuelo) temp[i]).darDiasOperacion();
						boolean estaElDia = false;
						for (int j = 0; j < diasOper.length && !estaElDia; j++) 
						{
							if(diasOper[j] != null)
							{
								if(diasOper[j].equals(dia) || diasOper[j].toLowerCase().equals(dia))
									estaElDia = true;
							}
						}
						if(estaElDia)
						{
							arcos[dondeVa] = temp[i];
							dondeVa++;							
						}
					}
				}	
			}
			else
			{
				E[] temp2 = (E[]) ((Ciudad) nodoFinal).darArcos();
				for (int j = 0; j < temp2.length && temp2[j] != null; j++) 
				{
					String[] diasOper = ((Vuelo) temp2[j]).darDiasOperacion();
					boolean estaElDia = false;
					for (int a = 0; a < diasOper.length && !estaElDia; a++) 
					{
						if(diasOper[a] != null)
						{
							if(diasOper[a].equals(dia) || diasOper[a].toLowerCase().equals(dia))
								estaElDia = true;
						}
					}
					if(dondeVa < 1600 && estaElDia)
					{
						arcos[dondeVa] = temp2[j];
						dondeVa++;
					}
					else
					{
						dondeVa = 0;
						cuantas++;
						if(cuantas > 10)
							siga = false;
					}
				}
				boolean ok = false;
				for (int h = 0; h < temp2.length && temp2[h] != null && !ok; h++) 
				{
					V finalActual = (V) temp2[h].darNodoFin();
					for (int q = 0; q < marcados.length && !ok; q++) {
						if(marcados[q] != null)
						{
							if(!marcados[q].equals(finalActual.darId()))
								ok = true;
						}
					}
				}
				if(ok == false)
					siga = false;
			}

			int aqui = 0;
			double costoMinimo = 5000;
			boolean fin = false;
			for (int i = 0; i < arcos.length && !fin && arcos[i] != null; i++) 
			{
				boolean caeACiclo = false;
				if(arcos[i].darCosto() < costoMinimo)
				{
					for (int j = 0; j < marcados.length && marcados[j] != null; j++) 
					{

						if((arcos[i].darNodoFin().darId() + (arcos[i].darCosto()+"")).equals(marcados[j]))
						{
							caeACiclo = true;
						}	
					}
					if(!caeACiclo)
					{
						costoMinimo = arcos[i].darCosto();
						nodoInicio =  (V) arcos[i].darNodoInicio();
						marcados[count] = (String)nodoInicio.darId() + (costoMinimo+"");
						count++;
						nodoFinal = (V) buscarNodo((String)arcos[i].darNodoFin().darId());
						marcados[count] = (String)nodoFinal.darId() + costoMinimo;
						aqui = i;
						count++;								
					}
				}
			}
			if(costoMinimo < 50)
			{	
				boolean esta = false;
				for (int p = 0; p < ciudadesMarcadas.length && !esta; p++) 
				{
					if(((String)nodoFinal.darId()).equals(ciudadesMarcadas[p]))
						esta = true;
				}
				if(!esta)
				{
					pesosMinimos[indicador] = "Ciudad origen: " + nodoInicio.darId() + ", ciudad destino: " + nodoFinal.darId() + ", costo (tiempo de vuelo) -> " +  (costoMinimo+"") + ", aerolinea: " + ((Vuelo) arcos[aqui]).darAerolinea().darNombre() + ", n�mero del vuelo: " + ((Vuelo) arcos[aqui]).darId() + ", hora del vuelo: " + ((Vuelo) arcos[aqui]).darHoraSalida() + ", d�a del vuelo: " + dia;
					ciudadesMarcadas[ind] = (String) nodoInicio.darId();
					ind++;
					ciudadesMarcadas[ind] = (String) nodoFinal.darId();
					ind++;
					arcos[aqui] = null;
					indicador++;
				}
			}
		}
		return pesosMinimos;
	}

	public String[][] darRutasEntre(Ciudad cOrigen, Ciudad cDestino) 
	{
		rutas = new Tupla[92][92];
		Tupla[][] retorno = darEsasRutas(cOrigen,cDestino, 0, 0);
		System.out.println(retorno);
		for (int i = 0; i < 92; i++) {
			for (int j = 0; j < 92; j++) {
				if(retorno[i][j] != null)
				System.out.println(retorno[i][j].imprimir());
				else
					System.out.println(retorno[i][j]);
			}
		}
		return null;
	}

	private Tupla[][] darEsasRutas(Ciudad cOrigen, Ciudad cDestino, int fila, int columna) 
	{
		V[] adyacentes = (V[]) cOrigen.darAdyacentes();
		Tupla t = null;
		boolean encontro = false;
		for (int i = 0; i < adyacentes.length && !encontro; i++) 
		{
			if(adyacentes[i] != null)
			{
				if(adyacentes[i].darId().equals(cDestino.darId()))
				{
					t = new Tupla((String)cOrigen.darId(), (String)cDestino.darId());
					rutas[fila][columna] = t;
					encontro = true;
					columna++;
				}
			}
		}
		//La ciudad buscada no esta entre estos adyacentes
		if(t == null)
		{
			for (int i = 0; i < adyacentes.length && adyacentes[i] != null; i++) 
			{
				Ciudad nuevaCiudadOrigen = (Ciudad) adyacentes[i];
				return darEsasRutas(nuevaCiudadOrigen, cDestino, fila, columna);
			}
			return rutas;
		}
		else
			return rutas;
	}

}
