package Estructuras;

public class Tupla 
{
	private String inicial;
	private String fin;
	
	public Tupla(String pinicial, String pfinal) 
	{
		inicial = pinicial;
		fin = pfinal;
	}

	public String imprimir() {
		String cadena = inicial+ " -> " + fin;
		return cadena;
	}

}
