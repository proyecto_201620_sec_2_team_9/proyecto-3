package Mundo;

public class Aerolinea 
{
	private String nombre;
	private int indicador;
	private int cantidadVuelos;
	private Vuelo[] vuelos;
	private double valorPorMinuto;
	private int numSillasValorMaxPorMinuto;
	private Ciudad[] ciudadesAerolinea;
	
	public Aerolinea(String pnombre, int numVuelos, double pValorPorMinuto, int pNumSillasValorMaxPorMinuto)
	{
		nombre = pnombre;
		cantidadVuelos = numVuelos;
		vuelos = new Vuelo[cantidadVuelos*2];
		valorPorMinuto = pValorPorMinuto;
		numSillasValorMaxPorMinuto = pNumSillasValorMaxPorMinuto;
		indicador = 0;
	}
	
	public void agregarVuelo(Vuelo nuevoVuelo)
	{
		vuelos[indicador] = nuevoVuelo;
		indicador++;
	}

	public Vuelo[] darVuelos()
	{
		return vuelos;
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public void eliminarVuelo()
	{
		
	}
	
	public void rutasMasCorta(String ciudadorigen, String ciudadDestino, String dia)
	{

		ciudadesAerolinea = new Ciudad[vuelos.length];
		int cont = 0;
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
			Ciudad x = vuelos[i].darCiudadOrigen();
			if(buscarCiudad(x.darNombre()) == null)
			{
				ciudadesAerolinea[cont] = x;
				cont++;
			}
			}
		}
		if(buscarCiudad(ciudadorigen) != null && buscarCiudad(ciudadDestino) != null)
		{
		Vuelo primerVuelo = vuelosOperaDia(ciudadorigen, dia)[0];
		if(primerVuelo != null)
		{	
			Vuelo vueloX = primerVuelo;
			int contar = 1;
			boolean llego = false;

			Vuelo[] ruta = new Vuelo[vuelos.length];
			ruta[0] = vueloX;
			String destino = vueloX.darCiudadDestino().darNombre();
			int contador = 1;
			String[] dias = new String[7];
			dias[contador-1] = dia;
			while(!llego)
			{
				if(destino.equalsIgnoreCase(ciudadDestino))
				{
					llego = true;
				}
				else
				{

						boolean encuentra = false;
						Vuelo[] posibles =  vuelosCiudadOrigen(destino);
						for (int i = 0; i < posibles.length && !encuentra; i++) {
							if(posibles[i] != null)
							{
							if(vueloEntre(posibles[i].darCiudadDestino().darNombre(), ciudadDestino) != null)
							{
								ruta[contador] = vueloEntre(posibles[i].darCiudadDestino().darNombre(), ciudadDestino);
								contador++;
								llego = true;
								encuentra = true;
							}
							}
						}
						if(posibles[0] != null && !encuentra)
						{
							destino = posibles[0].darCiudadDestino().darNombre();
							ruta[contador] = posibles[0];
							contador++;
						}
					
				}
			}
			if(llego)
			{
				double costo = 0;
				for (int i = 0; i < ruta.length; i++) {
					if(ruta[i] != null)
					{
						System.out.println(ruta[i].darCiudadOrigen().darNombre()+"-" + ruta[i].darId()+"-"+ruta[i].darHoraSalida()+"-"+ruta[i].darCiudadDestino().darNombre()+"-"+ruta[i].darCosto());
						costo+= ruta[i].darCosto();
					}
				}
				System.out.println("Costo total viaje : " + costo);
			}
			}
		}
		
	}
	public void rutasDesdeCiudad(String ciudadOrigen)
	{
		ciudadesAerolinea = new Ciudad[vuelos.length];
		int cont = 0;
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
			Ciudad x = vuelos[i].darCiudadOrigen();
			if(buscarCiudad(x.darNombre()) == null)
			{
				ciudadesAerolinea[cont] = x;
				cont++;
			}
			}
		}
		if(buscarCiudad(ciudadOrigen) != null)
		{
			for (int i = 0; i < ciudadesAerolinea.length; i++) {
				if(ciudadesAerolinea[i] != null)
				{
				if(!(ciudadesAerolinea[i].darNombre().equalsIgnoreCase(ciudadOrigen)))
				{
					Vuelo v = vueloEntre(ciudadOrigen, ciudadesAerolinea[i].darNombre());
					if(v != null)
					{
						System.out.println(v.darCiudadOrigen().darNombre()+"-" + v.darId()+"-"+v.darHoraSalida()+"-"+v.darCiudadDestino().darNombre()+"-"+v.darCosto());
					}
				}
				}
			}
		}
	}
	
	//Devuelve el vuelo con menor costo entre la ciudad de origen y la de destino
	public Vuelo vueloEntre(String ciudadorigen, String ciudaddestino)
	{
		Vuelo x = null;
		double costo = 999999.9;
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
				if(vuelos[i].darCiudadOrigen().darNombre().equalsIgnoreCase(ciudadorigen) && vuelos[i].darCiudadDestino().darNombre().equalsIgnoreCase(ciudaddestino))
				{
					if(vuelos[i].darCosto() < costo)
					{
						x = vuelos[i];
						costo = vuelos[i].darCosto();
					}
				}
			}
		}
		return x;
	}
	public Vuelo[] vuelosCiudadOrigen(String ciudadOrigen)
	{
		Vuelo[] x = new Vuelo[vuelos.length];
		int cont = 0;
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
				if(vuelos[i].darCiudadOrigen().darNombre().equalsIgnoreCase(ciudadOrigen))
				{
					x[cont] = vuelos[i];
					cont++;
				}
			}
		}
		return x;
	}
	public Vuelo[] vuelosOperaDia(String ciudadOrigen,String dia)
	{
		Vuelo[] x = new Vuelo[vuelos.length];
		int cont = 0;
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
				if(vuelos[i].darCiudadOrigen().darNombre().equalsIgnoreCase(ciudadOrigen) && vuelos[i].operaDia(dia))
				{
					x[cont] = vuelos[i];
					cont++;
				}
			}
		}
		return x;
	}
	public void MST(String nombreCiudad)
	{
		ciudadesAerolinea = new Ciudad[vuelos.length];
		int cont = 0;
		int vuel= 0;
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
			vuel++;
			Ciudad x = vuelos[i].darCiudadOrigen();
			if(buscarCiudad(x.darNombre()) == null && vuelos[i].darTipoVuelo().equals("Nacional"))
			{
				ciudadesAerolinea[cont] = x;
				cont++;
			}
			}
		}
		if(buscarEnVuelos(nombreCiudad) != null)
		{
			Vuelo x = buscarEnVuelos(nombreCiudad);
			System.out.println("Ciudad Origen: " + x.darCiudadOrigen().darNombre() +", ciudad destino: " + x.darCiudadDestino().darNombre() + ", costo del vuelo :" +x.darCosto() + ", numero del vuelo : "+ x.darId() + ", hora del vuelo :" + x.darHoraSalida());
		Ciudad[] visitados = new Ciudad[cont];
		Vuelo[] vuelosVisitados = new Vuelo[vuel];
		int vistado = 0;
		visitados[0] = x.darCiudadOrigen();
		visitados[1] = x.darCiudadDestino();
		int vis = 2;
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
				Ciudad v = vuelos[i].darCiudadOrigen();
				if(!vuelosYavistos(vuelosVisitados, vuelos[i]))
				{
					vuelosVisitados[vistado] = vuelos[i];
					vistado++;
				if(buscarEnLista(visitados, vuelos[i].darCiudadDestino().darNombre()) == null)
				{
					System.out.println("Ciudad Origen: " + v.darNombre() +", ciudad destino: " + vuelos[i].darCiudadDestino().darNombre() + ", costo del vuelo :" + vuelos[i].darCosto() + ", numero del vuelo : "+ vuelos[i].darId() + ", hora del vuelo :" + vuelos[i].darHoraSalida());
					visitados[vis] = vuelos[i].darCiudadOrigen();
				}
				}
			}
		}
		}
		else
		{
			System.out.println("No existe un vuelo asociado a esa ciudad en esta aerolinea");
		}
	}
	public boolean vuelosYavistos(Vuelo[] vuelos, Vuelo x)
	{
		boolean no = false;
		for (int i = 0; i < vuelos.length && !no; i++) {
			if(vuelos[i] != null)
			{
				if(vuelos[i].darCiudadOrigen().darNombre().equals(x.darCiudadOrigen().darNombre()) && vuelos[i].darCiudadDestino().darNombre().equals(x.darCiudadDestino().darNombre()))
				{
					no = true;
				}
			}
		}
		return no;
	}
	public Vuelo buscarEnVuelos(String nombre)
	{
		Vuelo rta = null;
		boolean encontro = false;
		for (int i = 0; i < vuelos.length && !encontro; i++) {
			if(vuelos[i] != null)
			{
			if(vuelos[i].darCiudadOrigen().darNombre().equalsIgnoreCase(nombre) && vuelos[i].darTipoVuelo().equals("Nacional"))
			{
				rta = vuelos[i];
				encontro = true;
			}
			}
		}
		return rta;
	}
	
	public Ciudad buscarEnLista(Ciudad[] x, String nombre)
	{
		Ciudad rta = null;
		boolean encontro = false;
		for (int i = 0; i < x.length && !encontro; i++) {
			if(x[i] != null)
			{
				if(x[i].darNombre().equals(nombre))
				{
					rta = x[i];
					encontro = true;
				}
			}
		}
		return rta;
	}
	public String dfs()
	{
		String rta = "(";
		ciudadesAerolinea = new Ciudad[vuelos.length];
		int cont = 0;
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
			Ciudad x = vuelos[i].darCiudadOrigen();
			if(buscarCiudad(x.darNombre()) == null && vuelos[i].darTipoVuelo().equals("Nacional"))
			{
				ciudadesAerolinea[cont] = x;
				rta += ciudadesAerolinea[cont].darNombre()+",";
				cont++;
			}

			}
		}
		return rta;
	}

	public Ciudad buscarCiudad(String nombre)
	{
		Ciudad rta = null;
		boolean encontro = false;
		for (int i = 0; i < ciudadesAerolinea.length && !encontro; i++) {
			if(ciudadesAerolinea[i] != null)
			{
				if(ciudadesAerolinea[i].darNombre().equalsIgnoreCase(nombre))
				{
					rta = ciudadesAerolinea[i];
					encontro = true;
				}
			}
		}
		return rta;
	}
	public double darValorPorMinuto()
	{
		return valorPorMinuto;
	}
	
	public int darNumSillasValorMaxPorMinuto()
	{
		return numSillasValorMaxPorMinuto;
	}

	public int darCantidadDeVuelos() 
	{
		return cantidadVuelos;
	}
	
	public Vuelo[][] darRutaMinimaRestoCiudades(String ciudad) 
	{
		Vuelo[][] respuesta = new Vuelo[91][91];
		int fila = 0;
		int columna = 0;
		int contador0 = 0;
		
		for (int i = 0; i < vuelos.length; i++) 
		{
			if(vuelos[i] != null)
			{
				String destinoActual = vuelos[i].darCiudadDestino().darNombre();
				System.out.println(destinoActual);
					boolean siga = true;
					String referencia = ciudad;
					columna = 0;
					while(siga)
					{
						Vuelo necesito = null;
						boolean encontro = false;
						for (int j = 0; j < vuelos.length && !encontro; j++) {
							if(vuelos[j] != null)
							{
								if(vuelos[j].darCiudadOrigen().darNombre().equalsIgnoreCase(referencia))
								{
									necesito = vuelos[j];
									encontro = true;
								}
							}
						}
						if(fila > 90 || columna > 90)
						{
							siga = false;
							necesito = null;
						}
						if(necesito != null)
						{
							respuesta[fila][columna] = necesito;
							columna++;
							referencia = necesito.darCiudadDestino().darNombre();
						}
						if(referencia.equalsIgnoreCase(destinoActual))
							siga = false;
					}
					
					fila++;
			}
		}
		
		return respuesta;
	}
}
