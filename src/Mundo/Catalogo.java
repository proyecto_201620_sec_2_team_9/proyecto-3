package Mundo;

public class Catalogo 
{
	//El catalogo tiene aerolineas
	private Aerolinea[] aerolineas;
	private Vuelo[] vuelos;
	private int indicadorVuelos;
	private int indicadorAerolineas;
	private int cantidadAerolineas;
	private int cantidadVuelos;
	
	public Catalogo()
	{
		
		cantidadAerolineas = 0;
		cantidadVuelos = 0;
		aerolineas = new Aerolinea[125];
		vuelos =  new Vuelo[3000];
		indicadorAerolineas = 0;
		indicadorVuelos = 0;
	}
	
	public void agregarVuelo(Vuelo nuevoVuelo)
	{
		vuelos[indicadorVuelos] = nuevoVuelo;
		indicadorVuelos++;
		cantidadVuelos++;
	}
	
	public void agregarAerolinea(Aerolinea nuevaAerolinea)
	{
		aerolineas[indicadorAerolineas] = nuevaAerolinea;
		cantidadAerolineas++;
		indicadorAerolineas++;
	}
	public void actualizarTarifas()
	{
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			vuelos[i].CalcularTarifa();
		}
	}
	public void eliminarVuelo(Vuelo vueloEliminar)
	{
		
	}
	
	public boolean eliminarAerolinea(String nombre)
	{
		boolean encontro = false;
		for (int i = 0; i < aerolineas.length && !encontro; i++) 
		{
			if(aerolineas[i] != null)
			{
				if(aerolineas[i].darNombre().equals(nombre) || aerolineas[i].darNombre().equals(nombre.toUpperCase()))
				{
					eliminarVuelosAerolinea(aerolineas[i].darNombre());
					aerolineas[i] = null;
					cantidadAerolineas--;
					encontro = true;
				}
			}
		}
		return encontro;
	}
	
	public void eliminarVuelosAerolinea(String darNombre) 
	{
		for (int i = 0; i < vuelos.length; i++) {
			if(vuelos[i] != null)
			{
				if(vuelos[i].darAerolinea().darNombre().equals(darNombre))
				{
					vuelos[i] = null;
					cantidadVuelos--;
				}
			}
		}
	}

	public int darCantidadAerolineas()
	{
		return cantidadAerolineas; 
	}

	public Aerolinea[] darAerolineas() 
	{
		return aerolineas;
	}

	public Vuelo[] darVuelos() 
	{
		return vuelos;
	}

	public Aerolinea buscarAerolinea(String nombreAerolinea) 
	{
		boolean encontro = false;
		Aerolinea buscada = null;
		for (int i = 0; i < aerolineas.length && !encontro; i++) 
		{
			if(aerolineas[i] != null)
			{
				if(aerolineas[i].darNombre().equals(nombreAerolinea) || aerolineas[i].darNombre().equals(nombreAerolinea.toUpperCase()))
				{
					buscada = aerolineas[i];
					encontro = true;
				}
			}
		}
		return buscada;
	}

	public int darNumeroDeVuelos() 
	{
		return cantidadVuelos;
	}

	public Vuelo[] darVuelosAerolinea(String nombre) 
	{
		Aerolinea buscada = buscarAerolinea(nombre);
		if(buscada != null)
		{
		return buscada.darVuelos();
		}
		else
		{
			return null;
		}
	}

	public void aumentarAerolineas() {
		cantidadAerolineas++;
		
	}

	public void MST(String nombreAerolinea,String nombreCiudad)
	{
		boolean encontro = false;
		for (int i = 0; i < aerolineas.length && !encontro; i++) {
			if(aerolineas[i] != null)
			{
				if(aerolineas[i].darNombre().equalsIgnoreCase(nombreAerolinea))
				{
					aerolineas[i].MST(nombreCiudad);
					encontro = true;
				}

			}
		}
		if(!encontro)
		{
			System.out.println("No existe una aerolinea con ese nombre");
		}
	}
	public void darComponentesConectadosAerolineas() {
		for (int i = 0; i < aerolineas.length; i++) {
			if(aerolineas[i] != null)
			{

			System.out.println("aerolinea : " + aerolineas[i].darNombre());
			System.out.println("{"+aerolineas[i].dfs()+"}");
			}
		}
		
	}
	
	public void darRuta(String ciudadOrigen, String ciudadDestino, String dia)
	{
		for (int i = 0; i < aerolineas.length; i++) {
			if(aerolineas[i] != null)
			{
				System.out.println("AEROLINEA :" + aerolineas[i].darNombre());
					aerolineas[i].rutasMasCorta(ciudadOrigen, ciudadDestino, dia);
			}
		}
	}
	public void darRutaAerolinea(String ciudadOrigen, String nombreAerolinea)
	{
		boolean encontro = false;
		for (int i = 0; i < aerolineas.length && !encontro; i++) {
			if(aerolineas[i] != null)
			{
				if(aerolineas[i].darNombre().equalsIgnoreCase(nombreAerolinea))
				{
					aerolineas[i].rutasDesdeCiudad(ciudadOrigen);
					encontro = true;
				}

			}
		}
		if(!encontro)
		{
			System.out.println("No existe una aerolinea con ese nombre");
		}
	}
}
