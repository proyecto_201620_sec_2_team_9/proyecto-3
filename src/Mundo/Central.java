package Mundo;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;

import Estructuras.Grafo;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Central {

	private Catalogo catalogo;
	private Grafo<Ciudad, Vuelo> grafo;
	String [][] aerolineasNumvuelos;
	String path;
	Ciudad[] ciudades;
	int cont;

	public Central() 
	{
		grafo = new Grafo<>();
		ciudades = new Ciudad[4500];
		cont = 0;
	}

	public void cargarArchivo(String rutaArchivo) 
	{
		try 
		{
			catalogo = new Catalogo();
			aerolineasNumvuelos = new String[29][2];
			path = rutaArchivo;
			leerExcelHoja1(path);
			leerExcelHoja2(path);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	public void leerExcelHoja1(String rutaArchivo) throws IOException 
	{
		File inputWorkbook = new File(rutaArchivo);
		Workbook excel;
		int numVuelos = 0;
		int numAerolineas = 0;
		String aerolinea = null;
		int id = 0;
		Ciudad ciudadOrigen = null;
		Ciudad ciudadDestino = null;
		int contador = 0;
		int contador2 = 0;
		String horaSalida1 = null;
		String horaLlegada1 = null;
		String horaSalidaSiguiente = null;
		String horaLLegadaSiguiente = null;
		String tipoEquipo = null;
		int numeroDeSillas = 0;
		String[] diasOperacion = null;
		String tipo = null;
		boolean esLaPrimeraHoraDeSalida = false;
		boolean esLaPrimeraHoraDeLLegada = false;
		String horaDeSalidaDefinitva = null;
		String horaDeLlegadaDefinitva  = null;
		

		try
		{
			excel = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = excel.getSheet(0);
			for (int i = 1; i < sheet.getRows(); i++) 
			{
				for (int j = 0; j < sheet.getColumns(); j++) 
				{
					String dato = sheet.getCell(j, i).getContents();
					if(dato != "")
					{
						if(j == 0)
						{
							if(aerolinea != null)
							{
								aerolineasNumvuelos[numAerolineas-1][1] = numVuelos+"";
							}
							aerolinea = dato;
							aerolineasNumvuelos[numAerolineas][0] = aerolinea;
							numAerolineas++;
							numVuelos = 0;
						}
						if(j == 1)
						{
							id = Integer.parseInt(dato);
							contador = 0;
							contador2 = 0;
						}
						if(j == 2)
						{
							ciudadOrigen = new Ciudad(dato);
							agregarCiudad(ciudadOrigen);
					    }
						if(j == 3)
						{
							ciudadDestino = new Ciudad(dato);
							agregarCiudad(ciudadDestino);
						}
						if(j == 4)
						{
							if(contador < 1)
							{
								horaSalida1 = dato;
								esLaPrimeraHoraDeSalida = true;
								contador++;
							}
							else
							{
								horaSalidaSiguiente = dato;
								esLaPrimeraHoraDeSalida = false;
								contador++;
							}
						}
						if(j == 5)
						{
							if(contador2 < 1)
							{
								horaLlegada1 = dato;
								esLaPrimeraHoraDeLLegada = true;
								contador2++;
							}
							else
							{
								horaLLegadaSiguiente = dato;
								esLaPrimeraHoraDeLLegada =false;
								contador2++;
							}
						}
						if(j == 6)
						{
							tipoEquipo = dato;
						}
						if(j == 7)
						{
							numeroDeSillas = Integer.parseInt(dato);
							diasOperacion = new String[7];
						}
						if(j == 8)
						{
							diasOperacion[0] = "Lunes";
						}
						if(j == 9)
						{
							diasOperacion[1] = "Martes";
						}
						if(j == 10)
						{
							diasOperacion[2] = "Miercoles";
						}
						if(j == 11)
						{
							diasOperacion[3] = "Jueves";
						}
						if(j == 12)
						{
							diasOperacion[4] = "Viernes";
						}
						if(j == 13)
						{
							diasOperacion[5] = "Sabado";
						}
						if(j == 14)
						{
							diasOperacion[6] = "Domingo";
						}
						if(j == 15)
						{
							tipo = dato;
							numVuelos++;

							if(esLaPrimeraHoraDeSalida)
								horaDeSalidaDefinitva = horaSalida1;
							else
								horaDeSalidaDefinitva = horaSalidaSiguiente;
							if(esLaPrimeraHoraDeLLegada)
								horaDeLlegadaDefinitva = horaLlegada1;
							else
								horaDeLlegadaDefinitva = horaLLegadaSiguiente;

							if(numAerolineas == 29 && numVuelos == 8)
							{
								aerolineasNumvuelos[numAerolineas-1][1] = numVuelos+"";
							}
						}
					}

				}
			}
			grafo.guardarTamanioVertices(cont);
			agregarAlGrafoLasCiudades();
		}
		catch(BiffException e)
		{
			e.printStackTrace();
		}
	}

	public void agregarAlGrafoLasCiudades() 
	{
		for (int i = 0; i < ciudades.length; i++) 
		{
			if(ciudades[i] != null)
			grafo.agregarNodo(ciudades[i]);
		}
	}

	public void agregarCiudad(Ciudad ciudadOrigen) 
	{
		boolean pailas = false;
		for (int k = 0; k < ciudades.length && !pailas; k++) 
		{
			if(ciudades[k] != null)
			{
				if(ciudades[k].darNombre().equals(ciudadOrigen.darNombre()))
				pailas = true;
			}
		}
		if(!pailas)
		{
			ciudades[cont] = ciudadOrigen;
			cont++;
		}
	}
	public boolean eliminarCiudad(Ciudad ciudadOr)
	{
		boolean elimino = false;
		for (int i = 0; i < ciudades.length && !elimino; i++) {
			if(ciudades[i] != null)
			{
				if(ciudades[i].darNombre().equalsIgnoreCase(ciudadOr.darNombre()))
				{
					ciudades[i] = null;
					elimino = true;
					cont--;
				}
			}
		}
			
		return elimino;
	}
	public void leerExcelHoja2(String rutaArchivo) throws IOException, ParseException 
	{
		File inputWorkbook = new File(rutaArchivo);
		Workbook excel;
		String aerolinea = null;
		double valorMinuto = 0.0;
		int numSillasValorMaxPorMinuto = 0;
		int cantidadVuelos = 0;
		try
		{
			excel = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = excel.getSheet(1);
			for (int i = 1; i < sheet.getRows(); i++) 
			{
				for (int j = 0; j < sheet.getColumns(); j++) 
				{
					String dato = sheet.getCell(j, i).getContents();
					if(dato != "")
					{
						if(j == 0)
						{
							aerolinea = dato;
						}
						if(j == 1)
						{
							String valorPorMinuto = dato.split(" ")[0];
							valorMinuto = Double.parseDouble(valorPorMinuto);
						}
						if(j == 2)
						{
							numSillasValorMaxPorMinuto = Integer.parseInt(dato);
							boolean encontro = false;
							for (int i1 = 0; i1 < 29 && !encontro; i1++) 
							{
								if(aerolineasNumvuelos[i1][0].equals(aerolinea))
								{
									encontro = true;
									cantidadVuelos = Integer.parseInt(aerolineasNumvuelos[i1][1]);
								}
							}
							Aerolinea aerolineaNueva = new Aerolinea(aerolinea, cantidadVuelos, valorMinuto, numSillasValorMaxPorMinuto);
							catalogo.agregarAerolinea(aerolineaNueva);
						}
					}
				}
			}
			agregarVuelos();
		}
		catch(BiffException e)
		{
			e.printStackTrace();
		}
	}
	

	public boolean AgregarAerolinea(String aerolinea)
	{
		boolean x = false;
		Aerolinea aerolineaBuscada = catalogo.buscarAerolinea(aerolinea);
		if(aerolineaBuscada != null)
		{
			x = true;
			catalogo.aumentarAerolineas();
		}
		return x;
	}
	
	public int darNumeroCiudades()
	{
		return cont;
	}
	public void agregarVuelos() throws IOException, ParseException 
	{
		File inputWorkbook = new File(path);
		Workbook excel;
		int numVuelos = 0;
		int numAerolineas = 0;
		String aerolinea = null;
		int id = 0;
		Ciudad ciudadOrigen = null;
		Ciudad ciudadDestino = null;
		int contador = 0;
		int contador2 = 0;
		String horaSalida1 = null;
		String horaLlegada1 = null;
		String horaSalidaSiguiente = null;
		String horaLLegadaSiguiente = null;
		String tipoEquipo = null;
		int numeroDeSillas = 0;
		String[] diasOperacion = null;
		String tipo = null;
		boolean esLaPrimeraHoraDeSalida = false;
		boolean esLaPrimeraHoraDeLLegada = false;
		String horaDeSalidaDefinitva = null;
		String horaDeLlegadaDefinitva  = null;

		try
		{
			excel = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = excel.getSheet(0);
			for (int i = 1; i < sheet.getRows(); i++) 
			{
				for (int j = 0; j < sheet.getColumns(); j++) 
				{
					String dato = sheet.getCell(j, i).getContents();
					if(dato != "")
					{
						if(j == 0)
						{
							aerolinea = dato;
							numAerolineas++;
							numVuelos = 0;
						}
						if(j == 1)
						{
							id = Integer.parseInt(dato);
							contador = 0;
							contador2 = 0;
						}
						if(j == 2)
						{
							ciudadOrigen = new Ciudad(dato);
						}
						if(j == 3)
						{
							ciudadDestino = new Ciudad(dato);
						}
						if(j == 4)
						{
							if(contador < 1)
							{
								horaSalida1 = dato;
								esLaPrimeraHoraDeSalida = true;
								contador++;
							}
							else
							{
								horaSalidaSiguiente = dato;
								esLaPrimeraHoraDeSalida = false;
								contador++;
							}
						}
						if(j == 5)
						{
							if(contador2 < 1)
							{
								horaLlegada1 = dato;
								esLaPrimeraHoraDeLLegada = true;
								contador2++;
							}
							else
							{
								horaLLegadaSiguiente = dato;
								esLaPrimeraHoraDeLLegada =false;
								contador2++;
							}
						}
						if(j == 6)
						{
							tipoEquipo = dato;
						}
						if(j == 7)
						{
							numeroDeSillas = Integer.parseInt(dato);
							diasOperacion = new String[7];
						}
						if(j == 8)
						{
							diasOperacion[0] = "Lunes";
						}
						if(j == 9)
						{
							diasOperacion[1] = "Martes";
						}
						if(j == 10)
						{
							diasOperacion[2] = "Miercoles";
						}
						if(j == 11)
						{
							diasOperacion[3] = "Jueves";
						}
						if(j == 12)
						{
							diasOperacion[4] = "Viernes";
						}
						if(j == 13)
						{
							diasOperacion[5] = "Sabado";
						}
						if(j == 14)
						{
							diasOperacion[6] = "Domingo";
						}
						if(j == 15)
						{
							tipo = dato;
							numVuelos++;

							if(esLaPrimeraHoraDeSalida)
								horaDeSalidaDefinitva = horaSalida1;
							else
								horaDeSalidaDefinitva = horaSalidaSiguiente;
							if(esLaPrimeraHoraDeLLegada)
								horaDeLlegadaDefinitva = horaLlegada1;
							else
								horaDeLlegadaDefinitva = horaLLegadaSiguiente;
							
							Aerolinea[] aerolineas = catalogo.darAerolineas();
							Aerolinea buscada = null;
							for (int k = 0; k < aerolineas.length; k++) 
							{
								if(aerolineas[k] != null)
								{
									if(aerolineas[k].darNombre().equals(aerolinea))
										buscada = aerolineas[k];
								}
							}
							Vuelo nuevoVuelo = new Vuelo(id, buscada, ciudadOrigen, ciudadDestino, horaDeSalidaDefinitva, horaDeLlegadaDefinitva, tipoEquipo, numeroDeSillas, diasOperacion, tipo);
							Ciudad origen = (Ciudad) grafo.buscarNodo(ciudadOrigen.darNombre());
							if(!origen.contieneArco(nuevoVuelo))
							{
								origen.agregarArco(nuevoVuelo);
							}
							buscada.agregarVuelo(nuevoVuelo);
							catalogo.agregarVuelo(nuevoVuelo);
						}
					}

				}
			}
			agregarArcosAlGrafo();
		}
		catch(BiffException e)
		{
			e.printStackTrace();
		}
	}

	private void agregarArcosAlGrafo() throws ParseException 
	{
		grafo.guardarTamanioArcos(catalogo.darNumeroDeVuelos());
		Vuelo[] vuelos = catalogo.darVuelos();
		for (int i = 0; i < vuelos.length; i++) 
		{
			if(vuelos[i] != null)
			{
				vuelos[i].asignarCosto();
				grafo.agregarArco(vuelos[i]);
			}
		}
	}

	
	
	public boolean eliminarAerolinea(String nombre) 
	{
		Vuelo[] vuelosAerolinea = catalogo.darVuelosAerolinea(nombre);
		if(vuelosAerolinea != null)
		{
			for (int i = 0; i < vuelosAerolinea.length; i++) 
			{
				if(vuelosAerolinea[i]!=null)
			   grafo.eliminarArco(vuelosAerolinea[i].darId());	
			}
		}
		return catalogo.eliminarAerolinea(nombre);
	}
	public void actualizarTarifas()
	{
		catalogo.actualizarTarifas();
	}
	
	
	public int darNumeroAerolineas() 
	{
		return catalogo.darCantidadAerolineas();
	}

	public boolean agregarVueloAlCatalogo(String numVuelo, String nombreAerolinea, String ciudadOrigen, String ciudadDestino, String horaSalida, String horaLlegada, String tipoAvion, String cupoVuelo, String diasOperacion, String tipoVuelo) 
	{
		boolean agrego = false;
		Aerolinea buscada = catalogo.buscarAerolinea(nombreAerolinea);
		if(buscada == null)
		{
			System.out.println("No existe la aerolinea " + nombreAerolinea);
		}
		else
		{
			int id = Integer.parseInt(numVuelo);
			Ciudad ciudOrigen = new Ciudad(ciudadOrigen);
			Ciudad ciudDestino = new Ciudad(ciudadDestino);
			int pCupo = Integer.parseInt(cupoVuelo);
			String pDiasOperacion[] = diasOperacion.split(";");
			Vuelo nuevoVuelo = new Vuelo(id, buscada, ciudOrigen, ciudDestino, horaSalida, horaLlegada, tipoAvion, pCupo, pDiasOperacion, tipoVuelo);
			catalogo.agregarVuelo(nuevoVuelo);
			grafo.agregarArco(nuevoVuelo);
			agrego = true;
		}
		return agrego;
	}

	public int darNumeroVuelos() 
	{
		return catalogo.darNumeroDeVuelos();
	}

	public String[][] darComponentesConectados() 
	{
		return grafo.dfs();
	}
	public void darComponentesConectadosAerolineas()
	{
		catalogo.darComponentesConectadosAerolineas();
	}
	
	public void darRutaMasCorta(String ciudadOrigen, String ciudadDestino,String dia)
	{
		catalogo.darRuta(ciudadOrigen, ciudadDestino, dia);
	}
	
	public void darRutaCiudad(String nombreAerolinea,String nombreCiudad)
	{
		catalogo.darRutaAerolinea(nombreCiudad, nombreAerolinea);
	}
	public void darMSTAerolinea(String nombreAerolinea, String nombreCiudad)
	{
		catalogo.MST(nombreAerolinea, nombreCiudad);
	}
	public String[] darMSTDesdeCiudad(String ciudadOrigen) 
	{
		Ciudad ciudadInicio = (Ciudad) grafo.buscarNodo(ciudadOrigen);
		if(ciudadInicio == null)
		{
			return null;
		}
		else
		{
			return grafo.mstNodo(ciudadInicio);
		}
	}

	public String[] darMSTDesdeCiudadYDia(String origen, String dia) 
	{
		Ciudad ciudadInicio = (Ciudad) grafo.buscarNodo(origen);
		if(ciudadInicio == null)
		{
			return null;
		}
		else
		{
			return grafo.mstNodoInt(ciudadInicio, dia);
		}
	}
	
	public String[][] buscarSiEsta(String origen, String destino) 
	{
		Ciudad cOrigen = (Ciudad) grafo.buscarNodo(origen);
		Ciudad cDestino = (Ciudad) grafo.buscarNodo(destino);
		if(cOrigen == null || cDestino == null)
		{
			return null;
		}
		else
		{
			return grafo.darRutasEntre(cOrigen, cDestino);
		}
	}
	
	public Aerolinea buscarAerolinea(String aerolinea) 
	{
		return catalogo.buscarAerolinea(aerolinea);
	}
}
