package Mundo;
import Estructuras.Nodo;

public class Ciudad implements Nodo
{
	//Una ciudad tiene ciudades adyacentes
	//Los nodos del grafo serian ciudades
	/**
	 * Nombre de la ciudad
	 */
	private String nombre;
	/**
	 *  lista de ciudades con las que tiene un vuelo
	 */
	private Ciudad[] adyacentes;
	private Vuelo[] arcos;
	private int dondeVa;
	private int dondeVaArcos;
	
	/**
	 * Crea una ciudad
	 * @param pNombre nombre de una ciudad pNombre != null &v pNombre != ""
	 */
	public Ciudad(String pNombre)
	{
		nombre = pNombre;
		adyacentes = new Ciudad[91];
		arcos = new Vuelo[90];
		dondeVa = 0;
		dondeVaArcos = 0;
	}
	
	/**
	 * Retorna el nombre de la ciudad
	 * @return nombre de la ciudad
	 */
	public String darNombre()
	{
		return nombre;
	}
	/**
	 * Retorna las ciudades adyacentes a la ciudad
	 * @return lista de ciudades adyacentes
	 */
	public Ciudad[] darAdyacentes()
	{
		return adyacentes;
	}
	/**
	 * Agrega una ciudad a la lista de adyacencia
	 * <pre> La ciudad no debe estar en la lista de adyacencia</pre>
	 * @param ciudadAdyacente ciudad para agregar ciudadAdyacente != null
	 */
	public void agregarAdyacentes(Ciudad ciudadAdyacente)
	{
		adyacentes[dondeVa] = ciudadAdyacente;
		dondeVa++;
	}

	public void agregarArco(Vuelo vuelo)
	{
		arcos[dondeVaArcos] = vuelo;
		dondeVaArcos++;
	}

	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Comparable darId() 
	{
		return nombre;
	}

	public Vuelo[] darArcos() {
		// TODO Auto-generated method stub
		return arcos;
	}

	public boolean contieneArco(Vuelo nuevoVuelo) 
	{
		boolean devolver = false;
		for (int i = 0; i < arcos.length && !devolver; i++) {
			if(arcos[i] != null)
			{
				if(arcos[i].darCiudadDestino().darNombre().equals(nuevoVuelo.darCiudadDestino().darNombre()))
					devolver = true;
			}
		}
		return devolver;
	}
}
