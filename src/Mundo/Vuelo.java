package Mundo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import Estructuras.Arco;

public class Vuelo extends Arco
{
	
	//Los arcos del grafo serian conexiones entre ciudades, es decir, vuelos
	
	/**
	 * Identificador unico del vuelo
	 */
	private int id;
	/**
	 * Aerolinea a la que pertenece el vuelo
	 */
	private Aerolinea aerolinea;
	/**
	 * Ciudad de donde sale el vuelo
	 */
	private Ciudad ciudadOrigen;
	/**
	 * Ciudad a donde llega el vuelo
	 */
	private Ciudad ciudadDestino;
	/**
	 * Hora a la que sale el vuelo de la ciudad de origen
	 */
	private String horaSalida;
	/**
	 * Hora a la que llega el vuelo de la ciudad de destino
	 */
	private String horaLlegada;
	/**
	 * Tipo del avion que realizara el vuelo
	 */
	private String tipoAvion;
	/**
	 * Numero de sillas que posee el avion
	 */
	private int cupo;
	/**
	 * Dias en los que el vuelo esta disponible
	 */
	private String[] diasOperacion;
	/**
	 * Tarifa del vuelo de lunes a jueves
	 */
	private double tarifaNormal;
	/**
	 * Tarifa del vuelo viernes a domingo
	 */
	private static double tarifaFDS;
	/**
	 * Tipo de vuelo del avion, nacional o internacional
	 */
	private String tipoVuelo;

	/** Crea un vuelo
	 * <pre> la aerolinea, la ciudad de llegada, y la ciudad de destino existen
	 * No existe un vuelo con el mismo id. </pre>
	 * @param pId id unico del avion pId != "" && pId != null
	 * @param pAerolinea aerolinea a la que pertenece el avion pAerolinea != null
	 * @param cOrigen ciudad de donde sale el avion cOrigen != null
	 * @param cDestino ciudad a donde llega el avion cDestino != null && cDestino != cOrigen
	 * @param hSalida hora de partida del avion hSalida != null && hSalida != ""
	 * @param hLlegada hora de llegada del avion hLlegada hLlegada != null && hLlegada != ""
	 * @param pTipoAvion Tipo del avion pTipoAvion != null && pTipoAvion != ""
	 * @param pCupo cupo del avion para el vuelo pCupo > 0
	 * @param pDiasOperacion dias de operacion del vuelo pDiasOperacion != null
	 * @param pTipoVuelo tipo de vuelo del avion pTipoVuelo != null && pTipoVuelo != "" && (pTipoVuelo == "Nacional" ||  pTipoVuelo == "Internacional")
	 */
	public Vuelo(int pId, Aerolinea pAerolinea, Ciudad cOrigen, Ciudad cDestino, String hSalida, String hLlegada, String pTipoAvion, int pCupo, String[] pDiasOperacion, String pTipoVuelo)
	{
		super(cOrigen, cDestino, tarifaFDS);
		id =pId;
		aerolinea = pAerolinea;
		ciudadOrigen = cOrigen;
		ciudadDestino = cDestino;
		horaSalida = hSalida;
		horaLlegada = hLlegada;
		tipoAvion =pTipoAvion;
		cupo = pCupo;
		diasOperacion = pDiasOperacion;
		tipoVuelo = pTipoVuelo;
		CalcularTarifa();
	}
	
	/**
	 * Calcula la tarifa para el vuelo
	 */
	public void CalcularTarifa()
	{
		tarifaNormal = 0;
		tarifaFDS = 0;
		double tarifaMin = aerolinea.darValorPorMinuto();
		int minutos = 0;
		String[] horaInicio = horaSalida.split(":");
		String[] horaFin = horaLlegada.split(":");
		int horasX = Integer.parseInt(horaInicio[0]);
		int horasY = Integer.parseInt(horaFin[0]);
		int minutosX = Integer.parseInt(horaInicio[1]);
		int minutosY = Integer.parseInt(horaFin[1]);
		while(!(horasX == horasY))
		{
			minutos += 60;
			horasX++;
			horasX = horasX %24;
		}
		int min = Math.abs(minutosY - minutosX);
		minutos += min;
		int numSillasMax = aerolinea.darNumSillasValorMaxPorMinuto();
		if(operaNormal())
		tarifaNormal = tarifaMin*(numSillasMax/cupo)*minutos;
		if(operaFDS())
		tarifaFDS =tarifaMin*(numSillasMax/cupo)*minutos*1.3;
	}
	
	private boolean operaNormal(){
		boolean x = false;
		for (int i = 0; i < diasOperacion.length && !x; i++) {
			if(diasOperacion[i] != null)
			{
			    if(diasOperacion[i].equals("Lunes") || diasOperacion[i].equals("Martes") || diasOperacion[i].equals("Miercoles") || diasOperacion[i].equals("Jueves"))
				x = true;
			}
		}
		return x;
	}
	public boolean operaDia(String dia)
	{
		boolean x = false;
		for (int i = 0; i < diasOperacion.length && !x; i++) {
			if(diasOperacion[i] != null)
			{
				if(diasOperacion[i].equalsIgnoreCase(dia))
					x=true;
			}
		}
		return x;
	}

	public double darTarifa(String dia)
	{
		if(dia.equalsIgnoreCase("Lunes") || dia.equalsIgnoreCase("Martes") || dia.equalsIgnoreCase("Miercoles") || dia.equalsIgnoreCase("Jueves"))
		{
			
			return tarifaNormal;
		}
		else
		{
			return tarifaFDS;
		}
	}
	private boolean operaFDS()
	{
		boolean x = false;
		for (int i = 0; i < diasOperacion.length && !x; i++) 
		{
			if(diasOperacion[i] != null)
			{
				if(diasOperacion[i].equals("Viernes") || diasOperacion[i].equals("Sabado") || diasOperacion[i].equals("Domingo"))
					x = true;	
			}
		}
		return x;
	}
	/**
	 * Retorna la ciudad de origen
	 * @return ciudad de origen
	 */
	public Ciudad darCiudadOrigen()
	{
		return ciudadOrigen;
	}
	
	/**
	 * Retorna la ciudad de destino
	 * @return ciudad de destino
	 */
	public Ciudad darCiudadDestino()
	{
		return ciudadDestino;
	}
	/**
	 * Retorna la id del vuelo
	 * @return Id del vuelo
	 */
	public int darId()
	{
		return id;
	}
	/**
	 * Retorna la aerolinea del vuelo
	 * @return Aerolinea del vuelo
	 */
	public Aerolinea darAerolinea()
	{
		return aerolinea;
	}
	/**
	 * Retorna la hora de salida del vuelo
	 * @return Hora de salida
	 */
	public String darHoraSalida()
	{
		return horaSalida;
	}
	
	/**
	 * Retorna la hora de llegada del vuelo
	 * @return Hora de llegada
	 */
	public String darHoraLlegada()
	{
		return horaLlegada;
	}
	
	public String darTipoVuelo()
	{
		return tipoVuelo;
	}
	
	public String[] darDiasOperacion()
	{
		return diasOperacion;
	}

	public void asignarCosto() throws ParseException 
	{
		double hsal = Integer.parseInt(horaSalida.split(":")[0]);
		double msal = Integer.parseInt(horaSalida.split(":")[1]) * 0.01;
		double decSal = msal / 0.6; 
		double horaSalida = hsal + decSal;
		double hlleg = Integer.parseInt(horaLlegada.split(":")[0]);
		double mlleg = Integer.parseInt(horaLlegada.split(":")[1]) * 0.01;
		double decLleg = mlleg / 0.6; 
		double horaLlegada = hlleg + decLleg;
		
		double diferencia = horaLlegada - horaSalida;
		if((horaSalida > 10 && horaLlegada <10) || (horaSalida > 20 && horaLlegada <20))
		{
			horaLlegada = horaLlegada+24;
			diferencia = horaLlegada - horaSalida;
		}
		
		super.asignarCosto(diferencia);
	}
}
